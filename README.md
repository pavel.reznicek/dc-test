# DC test

This is a test of the Motorola 68000 assembler dc.b/dc.w/dc.l pseudo-instruction
used in a FreePascal inline `asm ... end;` block.

The reason of this test is that in the time of writing this 
(September 29th 2022), FPC 3.2.2 throws an error:
`Error: Internal error 200405011`

See the [FPC Forum post](https://forum.lazarus.freepascal.org/index.php/topic,60745.0.html).

**Edit (1st October 2022):**

Tried with FPC 3.2.2 and improved the assembler code. *Everything went well!*
