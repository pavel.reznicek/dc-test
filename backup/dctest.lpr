program dctest;

{$mode objfpc}{$H+}

{$APPTYPE GUI}


type
  TMyBuffer = packed array[0..13] of Char;
  PMyBuffer = ^tmybuffer;

label byte1;
label byte2;
label word1;
label word2;
label longword1;
label longword2;
label str1;
label str2;
label moving;

var
  b1: Byte;
  b2: Byte;
  w1: Word;
  w2: Word;
  l1: DWord;
  l2: DWord;
  ps1: PMyBuffer = @str1;
  ps2: PMyBuffer = @str2;
  mb1: TMyBuffer;
  mb2: TMyBuffer;
  pb1: PMyBuffer;
  pb2: PMyBuffer;
begin
  New(pb1);
  New(pb2);
  asm
    jmp moving
  byte1:
    dc.b 0, 0               // store two bytes
  byte2:
    dc.b $00, $00
  word1:
    dc.b $FF, $FF           // store other two bytes
  word2:
    dc.w $0001              // store a word
  longword1:
    dc.l $FFFFFFFF          // store a longword
  longword2:
    dc.l 5                  // store another longword
  str1:
    // store a string
    dc.b $48,$65,$6C,$6C,$6F,$20,$77,$6F,$72,$6C,$64,$21,$00,$00
  str2:
    // store another string
    dc.b $48,$6F,$77,$20,$61,$72,$65,$20,$79,$6F,$75,$3F,$00,$00
  moving:
    move.b byte1, b1
    move.b byte2, b2
    move.w word1, w1
    move.w word2, w2
    move.l longword1, l1
    move.l longword2, l2
    move.l ps1, a1
    move.l a1, pb1
    move.l ps2, pb2
  end;
  mb1 := pb1^;
  mb2 := pb2^;
  WriteLn('byte 1:   ', b1);
  WriteLn('byte 2:   ', b2);
  WriteLn('word 1:   ', w1);
  WriteLn('word 2:   ', w2);
  WriteLn('long 1:   ', l1);
  WriteLn('long 2:   ', l2);
  WriteLn('pchar 1:  ', PChar(pb1));
  WriteLn('pchar 2:  ', PChar(pb2));
  WriteLn('buffer 1: ', mb1);
  WriteLn('buffer 2: ', mb2);

  ReadLn;

end.
